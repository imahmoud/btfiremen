﻿using UnityEngine;
using RAIN.Core;
using RAIN.Serialization;
using System.Collections.Generic;

/// <summary>
/// An extention class for the commander to add custom code to the AI Character.
/// it is useful and necessary to insert modifications between steps in the AI process, which is not possible as a MonoBehaviour.
/// This custom element is used to expose behavior that used in the behavior tree for the commander.
/// </summary>
[RAINSerializableClass]
public class FireCommanderElement : FireManElement
{
    /// <summary>
    /// A reference to the firemen objects in the scene.
    /// </summary>
    [RAINSerializableField]
    private List<GameObject> _fireMenObjects = new List<GameObject>();

    /// <summary>
    /// A reference to the firemen elements.
    /// </summary>
    private List<FireManElement> _fireMen = new List<FireManElement>();


    /// <summary>
    /// Checks whether all the firemen loaded in the fire truck or not.
    /// </summary>
    public bool AreFireMenLoaded
    {
        get
        {
            for(int i = 0; i < _fireMen.Count; i++)
            {
                if (!_fireMen[i].IsTruckLoaded)
                    return false;
            }
            return true;
        }
    }

    /// <summary>
    /// override the start function, clear all firemen elements, and register the new elements of firemen.
    /// </summary>
    public override void Start()
    {
        base.Start();

        // And our firemen
        _fireMen.Clear();
        for (int i = 0; i < _fireMenObjects.Count; i++)
            _fireMen.Add(_fireMenObjects[i].GetComponentInChildren<AIRig>().AI.GetCustomElement<FireManElement>());
    }

    /// <summary>
    /// Loads firemen, giving each of them the nextlocation command.
    /// </summary>
    public void LoadFireMen()
    {
        // This lets our firemen know where we are heading (they will figure our their own targets)
        for (int i = 0; i < _fireMen.Count; i++)
            _fireMen[i].CommandTarget = CommandTarget;
    }

    /// <summary>
    /// Clears all the targets, for firemen, fire truck, and commander in this sequence.
    /// </summary>
    public void ClearTarget()
    {
        // Remove our firemen targets first
        for (int i = 0; i < _fireMen.Count; i++)
            _fireMen[i].CommandTarget = null;

        // Then our truck
        FireTruck.CommandTarget = null;

        // Now our own
        CommandTarget = null;
    }

    /// <summary>
    /// Attach the firemen to the truck, then give the firetruck the command to the next location.
    /// </summary>
    public override void LoadTruck()
    {
        base.LoadTruck();

        // Once we are in the truck, we are set to go (truck target is command + truck name)
        FireTruck.CommandTarget = CommandTarget;
    }
}
