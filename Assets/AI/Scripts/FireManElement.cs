﻿using UnityEngine;
using RAIN.Core;
using RAIN.Serialization;
using RAIN.Navigation.Targets;
using RAIN.Navigation;

/// <summary>
/// An extention class for the fireman to add custom code to the AI Character.
/// it is useful and necessary to insert modifications between steps in the AI process, which is not possible as a MonoBehaviour.
/// This custom element is used to expose behavior that used in the behavior tree.
/// </summary>
[RAINSerializableClass]
public class FireManElement : CustomAIElement
{
    /// <summary>
    /// A reference to the firetruck game object.
    /// </summary>
    [RAINSerializableField]
    private GameObject _fireTruckObject = null;

    /// <summary>
    /// A reference to the firetruck custom element.
    /// </summary>
    private FireTruckElement _fireTruck = null;

    /// <summary>
    /// A reference to the old parent, temporary, to change the parent of the AI character.
    /// </summary>
    private Transform _oldParent = null;

    /// <summary>
    /// Accessor to the FireTruck game object. Once set and not null, set the firetruck element to its element.
    /// </summary>
    public GameObject FireTruckObject
    {
        get
        {
            return _fireTruckObject;
        }

        set
        {
            // If it did not change, return.
            if (_fireTruckObject == value)
                return;

            // if it changed, set it to the new value.
            _fireTruckObject = value;

            // if the new value is null, do not set reference to the element. else, set it.
            if (_fireTruckObject == null)
                _fireTruck = null;
            else
                _fireTruck = _fireTruckObject.GetComponentInChildren<AIRig>().AI.GetCustomElement<FireTruckElement>();
        }
    }

    /// <summary>
    /// Getter to the firetruck element.
    /// </summary>
    public FireTruckElement FireTruck
    {
        get
        {
            return _fireTruck;
        }
    }

    /// <summary>
    /// Accessor to the CommandTarget which specify the targetcommand coming from the Commander to the ai character.
    /// </summary>
    public GameObject CommandTarget
    {
        get
        {
            return AI.WorkingMemory.GetItem<GameObject>("commandtarget");
        }

        set
        {
            AI.WorkingMemory.SetItem("commandtarget", value);

            if (value == null)
                Target = null;
            else
                Target = NavigationManager.Instance.GetNavigationTarget(value.name + "_" + AI.Body.name);
        }
    }

    /// <summary>
    /// Accessor to the Target variable, where the fireman should move to.
    /// </summary>
    public NavigationTarget Target
    {
        get
        {
            return AI.WorkingMemory.GetItem<NavigationTarget>("target");
        }

        set
        {
            AI.WorkingMemory.SetItem("target", value);
        }
    }

    /// <summary>
    /// Getter to tell if the truck is loaded or not.
    /// </summary>
    public bool IsTruckLoaded
    {
        get
        {
            return AI.WorkingMemory.GetItem<bool>("istruckloaded");
        }
    }

    /// <summary>
    /// Overriding the start method, setting references to fire truck object and fire truck elemnt.
    /// </summary>
    public override void Start()
    {
        base.Start();

        // Resolve our firetruck element
        _fireTruck = _fireTruckObject.GetComponentInChildren<AIRig>().AI.GetCustomElement<FireTruckElement>();

        // Set this for the behavior tree
        AI.WorkingMemory.SetItem("firetruck", _fireTruckObject);

    }

    /// <summary>
    /// Virtual method, to load the fire men in the fire truck.
    /// </summary>
    public virtual void LoadTruck()
    {
        // Turn off our renderers.
        Renderer[] tRenderers = AI.Body.GetComponentsInChildren<Renderer>();
        for (int i = 0; i < tRenderers.Length; i++)
            tRenderers[i].enabled = false;

        // Parent us to the truck
        _oldParent = AI.Body.transform.parent;
        AI.Body.transform.parent = _fireTruckObject.transform;

        // And loaded
        AI.WorkingMemory.SetItem("istruckloaded", true);
    }

    /// <summary>
    /// A virtual method to unload firemen from the fire truck.
    /// </summary>
    public virtual void UnLoadTruck()
    {
        // Turn renderers back on
        Renderer[] tRenderers = AI.Body.GetComponentsInChildren<Renderer>(true);
        for (int i = 0; i < tRenderers.Length; i++)
            tRenderers[i].enabled = true;

        // Unparent us 
        AI.Body.transform.parent = _oldParent;

        // And unloaded
        AI.WorkingMemory.SetItem("istruckloaded", false);
    }
}