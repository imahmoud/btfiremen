﻿using UnityEngine;
using RAIN.Core;
using RAIN.Serialization;
using RAIN.Navigation;
using RAIN.Navigation.Targets;

/// <summary>
/// An extention class for the firetruck to add custom code to its AI.
/// it is useful and necessary to insert modifications between steps in the AI process, which is not possible as a MonoBehaviour.
/// This custom element is used to expose behavior that used in the behavior tree.
/// </summary>
[RAINSerializableClass]
public class FireTruckElement : CustomAIElement
{
    /// <summary>
    /// Accessor to the CommandTarget which specify the targetcommand coming from the Commander to the firetruck.
    /// </summary>
    public GameObject CommandTarget
    {
        get
        {
            return AI.WorkingMemory.GetItem<GameObject>("commandtarget");
        }

        set
        {
            AI.WorkingMemory.SetItem("commandtarget", value);

            // Also set the value of Target by adding the name of the ai.
            if (value == null)
                Target = null;
            else
                Target = NavigationManager.Instance.GetNavigationTarget(value.name + "_" + AI.Body.name);
        }
    }

    /// <summary>
    /// Accessor to the Target variable, where the Firetruck should move to.
    /// When it set, arrived must be set to false.
    /// </summary>
    public NavigationTarget Target
    {
        get
        {
            return AI.WorkingMemory.GetItem<NavigationTarget>("target");
        }

        set
        {
            AI.WorkingMemory.SetItem("target", value);
            AI.WorkingMemory.SetItem("arrived", false);
        }
    }

    /// <summary>
    /// A getter to the arrived value.
    /// </summary>
    public bool Arrived
    {
        get
        {
            return AI.WorkingMemory.GetItem<bool>("arrived");
        }
    }
}