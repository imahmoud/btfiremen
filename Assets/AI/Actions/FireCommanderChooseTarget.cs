using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

/// <summary>
/// Custom action class, to select new situation randomly.
/// </summary>
[RAINAction]
public class FireCommanderChooseTarget : RAINAction
{
    /// <summary>
    /// A reference to the fire commander element.
    /// </summary>
    private FireCommanderElement _fireCommander = null;

    /// <summary>
    /// A list of all situations in the scene.
    /// </summary>
    private List<GameObject> _targets = new List<GameObject>();

    /// <summary>
    /// A variable to store the last situation.
    /// </summary>
    private int _lastTarget = -1;

    /// <summary>
    /// Start function, initial values. Then generate random number for random situation. Check if it is not the same as last situation.
    /// </summary>
    /// <param name="ai">A reference to the component</param>
    public override void Start(RAIN.Core.AI ai)
    {
        _fireCommander = ai.GetCustomElement<FireCommanderElement>();

        if(_targets.Count == 0)
        {
            _targets.Add(GameObject.Find("situationone"));
            _targets.Add(GameObject.Find("situationtwo"));
            _targets.Add(GameObject.Find("situationthree"));
            _targets.Add(GameObject.Find("situationfour"));
            _targets.Add(GameObject.Find("situationfive"));
        }

        int tNextTarget = Random.Range(0, 3);
        if (tNextTarget == _lastTarget)
            tNextTarget = (tNextTarget + 1) % _targets.Count;

        _lastTarget = tNextTarget;
        _fireCommander.CommandTarget = _targets[tNextTarget];
    }

    /// <summary>
    /// Executes the current custom action, generating a new random situation.
    /// </summary>
    /// <param name="ai"></param>
    /// <returns>SUCCESS if the execution completed correctly, FAILURE otherwise.</returns>
    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        if(_fireCommander.CommandTarget == null)
            return ActionResult.FAILURE;

        return ActionResult.SUCCESS;
    }    
}