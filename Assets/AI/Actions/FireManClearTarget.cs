using RAIN.Action;
using RAIN.Core;

/// <summary>
/// A custom action class to clear the current target situation from all the firemen, truck and commander.
/// </summary>
[RAINAction]
class FireManClearTarget : RAINAction
{
    /// <summary>
    /// A reference to the commander element.
    /// </summary>
    private FireCommanderElement _fireCommander = null;

    /// <summary>
    /// Set the reference to the commander, and issue the clear target command.
    /// </summary>
    /// <param name="ai">A reference to the ai component.</param>
    public override void Start(AI ai)
    {
        _fireCommander = ai.GetCustomElement<FireCommanderElement>();

        _fireCommander.ClearTarget();
    }
}