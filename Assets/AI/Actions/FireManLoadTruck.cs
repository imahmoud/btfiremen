using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

/// <summary>
/// Custom action class, to load firemen in truck.
/// </summary>
[RAINAction]
public class FireManLoadTruck : RAINAction
{
    /// <summary>
    /// A reference to the fire man
    /// </summary>
    private FireManElement _fireMan= null;

    public override void Start(RAIN.Core.AI ai)
    {
        _fireMan = ai.GetCustomElement<FireManElement>();

        _fireMan.LoadTruck();
    }

    /// <summary>
    /// Executes the current custom action
    /// </summary>
    /// <param name="ai">A reference to the ai component</param>
    /// <returns>keeps returning running, until it finishes, returns success</returns>
    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        if (_fireMan.IsTruckLoaded && _fireMan.FireTruck.Arrived)
        {
            _fireMan.UnLoadTruck();
            return ActionResult.SUCCESS;
        }

        return ActionResult.RUNNING;
    }
}