using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

/// <summary>
/// Custom action class, to load firemen with new location
/// </summary>
[RAINAction]
public class FireCommanderLoadFiremen : RAINAction
{
    /// <summary>
    /// A reference to the fire commander element.
    /// </summary>
    private FireCommanderElement _fireCommander = null;

    /// <summary>
    /// Start function, give the load firemen with target location command.
    /// </summary>
    /// <param name="ai">A reference to the component</param>
    public override void Start(RAIN.Core.AI ai)
    {
        _fireCommander = ai.GetCustomElement<FireCommanderElement>();

        _fireCommander.LoadFireMen();
    }

    /// <summary>
    /// Executes this custom action. 
    /// </summary>
    /// <param name="ai"></param>
    /// <returns>Running, while not finished, once finished returns success</returns>
    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        if (_fireCommander.AreFireMenLoaded)
            return ActionResult.SUCCESS;

        return ActionResult.RUNNING;
    }    
}